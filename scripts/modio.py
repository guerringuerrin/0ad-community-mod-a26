import json
import os
import requests

from . import MOD_PATH

api_key = os.getenv('MODIO_API_KEY')

# This must be generated manually from the website's interface.
oauth2_token = os.getenv("MODIO_OAUTH2_TOKEN")

mod_file_path = os.getenv("MOD_FILE_PATH")
mod_version = os.getenv("MOD_VERSION")

community_mod_id = 2144305
zeroad_id = 5

headers = {
    'Authorization': f'Bearer {oauth2_token}',
    'Accept': 'application/json'
}

r = requests.get(f'https://api.mod.io/v1/me/mods', params={
    'api_key': api_key
}, headers = headers)

print(r.json())

files = {'filedata': open(mod_file_path, 'rb')}

mod_json = json.load(open(MOD_PATH / 'mod.json', 'r'))

signature = open('signature.minisign', 'r').read()

rq = requests.post(f'https://api.mod.io/v1/games/{zeroad_id}/mods/{community_mod_id}/files', files=files, headers=headers, data={
    'version': mod_version,
    'active': True,
    'metadata_blob' : json.dumps({
        'dependencies': mod_json['dependencies'],
        'minisigs': [signature]
    })
})

print(rq.json())