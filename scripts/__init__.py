import json
from pathlib import Path

MOD_PATH = Path("community-mod")

def check_cwd_is_correct():
	cwd = Path().resolve()
	try:
		if not Path(".git").exists():
			raise Exception("No .git in current folder.")
		with open(MOD_PATH / "mod.json", "r") as f:
			mod = json.load(f)
			if mod['name'] != "community-mod":
				raise Exception("mod.json has incorrect name")
	except:
		raise Exception("scripts.py must be called from the community mod repository")
