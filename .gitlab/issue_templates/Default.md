## Summary

(Summarize the issue. You may also refer to earlier commits.)

## Possible Fixes

(Suggest ways that this could be fixed.)

## Replays

(If relevant, provide replays of 0 A.D. games showing your issue.)
