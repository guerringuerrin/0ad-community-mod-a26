[font="sans-bold-24"] Community-mod changelog
[font="sans-bold-20"]
#Version 10 (July 15, 2024)
[font="sans-16"]

- Woodlines a little larger in badosu maps
- Reduced cavalry counter damage: spearcav 2x to 1.75x, infantry 3x to 2.5x.
- Fixed athenian walls being invisible and impossible to kill.

[icon="forge" displace="0 8"][font="sans-bold-18"] Forge military techs
[font="sans-16"]
Decreased research time for military technologies

[icon="tech_melee-attack01"] [icon="tech_range-attack01"] [icon="tech_melee-resistance01"] [icon="tech_range-resistance01"]
[icon="icon_time"]: from 40 to 20.

[icon="tech_melee-attack02"] [icon="tech_range-attack02"] [icon="tech_melee-resistance02"] [icon="tech_range-resistance02"]
[icon="icon_time"]: from 50 to 40.


[icon="elephants" displace="0 8"][font="sans-bold-18"] Elephants
[font="sans-16"]
Increased Melee Elephants hack damage from 25 to 30
Decreased -1 pierce armor and +1 hack armor to slightly improve their durability.


[icon="rome_siege_scorpio" displace="0 8"][font="sans-bold-18"] Bolts
[font="sans-16"]
Reduce movement speed to 6.8.
Range from 90 to 80 meters.

[icon="emblem_spartans" displace="0 18"][font="sans-bold-18"] Spartans update
[font="sans-16"]
Implement borg's Spartans differentiation patch for balance testing in the community mod.


- Civ bonus 

[icon="civbonus_agoge" displace="0 8"][font="sans-bold-18"] Laws of Lycurgus
[font="sans-16"]
Champion Hoplites are available in [icon="phase_1"] Village Phase and may promote to Olympic champions.


- Units

[icon="spart_hero_agis" displace="0 8"] [font="sans-bold-18"] Agis Hero
[font="sans-16"]
Added "Great Revolt" aura: Soldiers -25% [icon="icon_metal"] and training time.
Added "Last Stand" aura: Agis +50% health.

[icon="spart_neodamodes" displace="0 8"] [font="sans-bold-18"] Neodamodes
[font="sans-16"]
New Infantry Spearman trained at the Barracks and Civic Center


- Technologies

[icon="tech_apophora" displace="0 8"][font="sans-bold-18"] Apophorá 
[font="sans-16"]
Infantry Javelineers +100% grain gather rate, but -20% attack. 
Cost: [icon="icon_food"] 200 [icon="icon_wood"] 200.

[icon="tech_krypteia" displace="0 8"][font="sans-bold-18"] Krypteia 
[font="sans-16"]
Champion Hoplites +15% melee attack damage, but Citizen Infantry Javelineers +30% training time. 
Cost: [icon="icon_food"] 400.

[icon="tech_tyrtean-paeans" displace="0 8"][font="sans-bold-18"] Tyrtean Paeans 
[font="sans-16"]
Champion Melee Infantry +10% movement speed. 
Cost: [icon="icon_food"] 400 [icon="icon_metal"] 200.

[icon="tech_unlock-neodamodes" displace="0 8"][font="sans-bold-18"] Unlock Neodamodes
[font="sans-16"]
Unlock the ability to train Spearman Neodamodes at the Barracks and Civic Center.
Cost: [icon="icon_food"] 300 [icon="icon_metal"] 200.


- Buildings

[icon="spart_gerousia" displace="0 8"] [font="sans-bold-18"] Gerousia
[font="sans-16"]
Train Heroes and research unique technologies.
Cost: [icon="icon_stone"] 100 [icon="icon_metal"] 100.

[icon="spart_syssition" displace="0 8"] [font="sans-bold-18"] Syssition
[font="sans-16"]
Train Champions and research unique technologies.
Cost: [icon="icon_stone"] 200 [icon="icon_metal"] 200.


[icon="emblem_carth" displace="0 18"][font="sans-bold-18"] Carthaginians update
[font="sans-16"]
Allow apartments in [icon="phase_1"] Village Phase.
Houses can be now promoted into apartments.

[icon="carth_apartment" displace="0 8"][font="sans-bold-18"] Apartments
[font="sans-16"]
Cost: from [icon="icon_wood"] 50 [icon="icon_stone"] 50 to [icon="icon_wood"] 25 [icon="icon_stone"] 50







[font="sans-bold-20"]
#Version 9 (June 18, 2024)
[font="sans-16"]
Non-random arrows behavior reverted to a26 vanilla arrows behavior.
Included badosu maps.


[icon="pers_ship_bireme" displace="0 8"][font="sans-bold-18"] Naval overhaul
[font="sans-16"]
Introduced the new naval combat system from a27.


[icon="rome_siege_ballista" displace="0 8"] [icon="rome_siege_scorpio" displace="0 8"][font="sans-bold-18"] Catapults and Bolts
[font="sans-16"]
Reduced pack time from 10 to 5 seconds.


[icon="kush_temple_amun" displace="0 8"][font="sans-bold-18"] Grand Temple of Amun
[font="sans-16"]
Temple of Amun capture points doubled.


[icon="pers_immortal_archer" displace="0 8"][font="sans-bold-18"] Immortals
[font="sans-16"]
Fixed ranged immortal cost to: [icon="icon_food"] 50 [icon="icon_wood"] 30 [icon="icon_metal"] 50.
Reduced weapon switch time from 6 to 4 seconds.


[icon="tech_loom" displace="0 8"][font="sans-bold-18"] The Loom
[font="sans-16"]
Cost: from [icon="icon_food"] 200 to [icon="icon_food"] 150.
[icon="icon_time"]: from 40 to 20.


[icon="wall" displace="0 8"][font="sans-bold-18"] Walls
[font="sans-16"]
Medium and large walls garrison doubled.



[font="sans-bold-20"]
#Version 7 (March 24, 2024)

[font="sans-bold-18"]#Unit Rebalancing
[font="sans-16"]
- Melee rank ups nerfed: +1 armor, +HP, +20% damage -> +HP, +10% damage.
- All mercenaries receive +10% damage baseline as compensation.
- Persian immortals moved to barracks, made cheaper and weaker.
- Elephants prefer 'Human' targets to buildings.

[font="sans-bold-20"]
- Archers and Crossbows
[font="sans-16"]
- Infantry Archers and Crossbows
- Pierce damage increased from 6.7 to 7.0
- Champion Archers 
- Pierce damage increased from 13.5 to 13.8
- Cavalry Archers
- Pierce damage increeased from 7 to 7.3
- Champion Cavalry Archers 
- Pierce damage increased from 14 to 14.1
- Elephants Archer 
- Pierce damage increased from 14 to 14.1

[font="sans-bold-20"]
- Elephants
[font="sans-16"]
- Hack resistance increased from 5  to  6
- Splash damage increased from 20 hack to 25 hack

[font="sans-bold-20"]
- Siege
[font="sans-16"]
- Turn angle increased from 45 degrees to 90 degrees
- Siege +1 hack armor, 
- Catapults and Bolt shooters decreased prepare time
- Catapult splash range increased from 1 to 1.5, splash damage slightly increased from 100 to 120.

[font="sans-bold-20"]
- Heroes
[font="sans-16"]
- Iphicrates nerfed: Armor decreased from 3 to 2, Aura uses a range instead of formation.

[font="sans-bold-18"]#Buildings
[font="sans-16"]
- Reduced garrisoned arrows
- Sentry Towers wood cost increased by 25
- Buildings -1 hack armor
- Siege towers and ships may now target their arrows.



[font="sans-bold-20"]
#Version 6 (Jan 28, 2024)


[font="sans-bold-18"]#Infantry
[font="sans-16"]
- Melee units damage increased by +50%, decreased armor
- Ranged units damage decreased -25%
- Melee infantry  base movement speed slighty increased (9.5 ms)
- Clubmen get a small hack attack
- Buildings and siege adjusted for higher hack damage


[font="sans-bold-20"]#Buildings AI attack behavior
[font="sans-16"]
- Buildings targets closest units by default
- Target unit using Right Click
- Rally point on unit using Ctrl Click
- Accuracy for cc and forts reduced.


[font="sans-bold-20"]#Han fixes
[font="sans-16"]
- Poison arrows tech cost changed to 1200 food 1200 metal
- Fields easier to place
- Elephants get a splash attack
- Upgraded CC batch time reduction nerfed from 0.5 to 0.75


[font="sans-bold-20"]#Mauryans Heroes
[font="sans-16"]
- Chanakya slightly lower discount. Doesn't need to garrison for tech discount
- Chandragupta global elephants bonus for speed and attack rate, local armor bonus for all soldiers
- Elephants get a splash attack


[font="sans-bold-20"]#Catapults
[font="sans-16"]
- Smaller range
- Gain a splash attack


[font="sans-bold-20"]
#Updated Version 4 (Jun 21, 2023)

[font="sans-16"]
- Add splash damage to all military ships

[font="sans-bold-20"]
#Updated Version 4 (Mar 31, 2023)
[font="sans-16"]

- Reduce all units damage by 25%
- Increase melee infantry speed by 0.5
- Champion Infantry movement speed set to 9.5
- Infantry Heroes movement speed increased to 9.5
- Elephants Pierce damage decreased from 14 to 10
- Han Han Minister Hack damage increased from 10 to 16


[font="sans-bold-20"]
#Updated Version 4 (Mar 26, 2023)

[font="sans-bold-20"]#Final unit rebalancing adjustments

- Infantry
[font="sans-16"]
- Maceman 
- Crush damage decreased from 9 to 7
- Pikeman
- Hack damage decreased from 6 to 5
- Hack resistance decreased from 6 to 5
- Pikeman
- Hack damage decreased from 2 to 1

[font="sans-bold-20"]
- Champion Infantry
[font="sans-16"]
- Maceman 
- Crush damage decreased from 18 to 14
- Pikeman
- Hack damage decreased from 12 to 10
- Hack resistance decreased from 7 to 5
- Trumpeter
- Hack damage increased from 10 to 22
- Hack resistance increased from 1 to 2
- Pierce resistance increased from 1 to 2

[font="sans-bold-20"]
- Cavalry
[font="sans-16"]
- Melee base hack damage decreased from 3 to 2

- Axeman
- Hack resistance increased from 2 to 3
- Maceman
- Crush damage decreased from 9.5 to 7.5
- Hack damage increased from 6 to 8
- Hack damage decreased from 8 to 7.5
- Hack resistance decreased from 3 to 2
- Spearman
- Hack damage increased from 7.5 to 7.7
- Pierce damage increased from 6.25 to 6.5
- Swordsman
- Pierce resistance increased from 2 to 2.5

[font="sans-bold-20"]
- Champion Cavalry
[font="sans-16"]
- Axeman
- Hack resistance of 1 added
- Maceman
- Crush damage decreased from 19 to 15
- Hack damage increased from 12 to 15
- Pierce resistance removed
- Spearman
- Hack damage increased from 15 to 15.5
- Pierce damage increased from 12.5 to 13
- Swordsman
- Pierce resistance of 0.5 added


[font="sans-bold-20"]
#Updated Version 4 (Mar 25, 2023)

[font="sans-bold-20"]#Major units rebalancing
[font="sans-bold-20"]
- Infantry
[font="sans-16"]
- Melee base hack and pierce damage of 3

- Axeman 
- Hack damage increased from 6 to 12
- Crush damage increased from 2 to 4
- Hack resistance decreased from 4 to 2
- Pierce resistance removed
- Maceman
- Crush damage increased from 7 to 9
- Hack damaged of 7 added
- Hack resistance decreased from 4 to 2
- Pierce resistance removed
- Pikeman
- Hack damage increased from 2 to 6
- Crush damage increased from 3 to 9
- Hack resistance decreased from 10 to 6
- Pierce resistance decreased from 8 to 5
[font="sans-bold-20"]
- Champion Infantry
[font="sans-16"]
- Base hack damage decreased from 5 to 3
- Base pierce damage decreased from 5 to 3

- Axeman 
- Hack damage increased from 12 to 24
- Crush damage increased from 4 to 8
- Maceman 
- Hack damage of 14 added
- Crush damage increased from 14 to 18
- Hack resistance increased from 1 to 2
- Pierce resistance increased from 1 to 3
- Pikeman
- Hack damage increased from 4 to 12
- Pierce damage increased from 6 to 18
- Hack resistance decreased from 8 to 7
- Pierce resistance decreased from 6 to 5
- Spearman
- Hack damage increased from 6 to 12
- Pierce damage increased from 5 to 10
- Slightly slower repeat attack time from 900 to 1000
- Swordsman
- Hack damage decreased from 11 to 22
[font="sans-bold-20"]
- Cavalry
[font="sans-16"]
- Melee base hack damage decreased from 3 to 2

- Axeman
- Hack damage increased from 8.7 to 14
- Crush damage increased from 2.3 to 4.5
- Hack resistance decreased from 3 to 2
- Maceman
- Crush damage increased from 8 to 9.5
- Hack damage of 6 added
- Hack resistance decreased from 4 to 3
- Swordsman
- Hack damage increased from 6.5 to 12
- Pierce resistance decreased from 4 to 2
[font="sans-bold-20"]
- Champion Cavalry
[font="sans-16"]
- Base hack damage decreased from 7 to 5

- Axeman
- Hack damage increased from 13.8 to 28
- Crush damage increased from 4.6 to 8
- Pierce resistances of 2 added
- Maceman
- Crush damage increased from 16 to 19
- Hack damage of 12 added
- Spearman
- Hack damage increased from 8 to 15
- Pierce damage increased from 6 to 12.5
- Hack resistance increased from 1 to 2
- Pierce resistance decreased from 2 to 1
- Swordsman
- Hack damage increased from 13 to 24
- Pierce resistance removed
- Hack resistance of 1 added


[font="sans-bold-20"]
#Version 4 (Dec 16, 2022)


[font="sans-bold-20"]#Cavalry
[font="sans-16"]
- Increase Spear Cavalry acceleration by 15%%

[font="sans-bold-20"]#Civilizations
[font="sans-16"]
- Add Seleucids civilization bonus: Farms 25% wood cost and 75% build time, no diminishing returns for increased field workers.
- Reduced Romans Army Camp and Health. 500/100/100 (Wood/Stone/Metal) to 400/150 (Wood/Stone) Max HP from 2250 to 1750
- Romans Army Camp available in Town Phase


[font="sans-bold-20"]#Infantry
[font="sans-16"]
- Add Roman Centurion as Upgrade from Rank 3 Soldiers. 
- Upgradable at a cost of 100/50 (Food/Metal) from Rank 3 Swordsmen and Spearmen. 12 seconds upgrade time
- Aura: Melee Soldiers +10% attack damage and all soldiers -25% experience points for promotion.


[font="sans-bold-20"]
#Version 3 (Nov 15, 2022)


[font="sans-bold-20"]#Healers
[font="sans-16"]
- Decrease Healers training cost from 250/50 to 100/25 (Food/Metal)
- Decrease Healing Range I tech cost from 400/200 to 200/100 (Food/Metal)
- Decrease Healing Range II tech cost from 800/400 to 300/150 (Food/Metal)
- Decrease Healing Rate I tech cost from 500/250 to 200/100 (Food/Metal)
- Decrease Healing Rate I tech cost from 1000/500 to 300/150 (Food/Metal)


[font="sans-bold-20"]#Cavalry
[font="sans-16"]
- Decrease Axe Cavalry Crush damage from 2.9 to 2.3
- Increase Axe Cavalry Hack damage from 6.9 to 8.7


[font="sans-bold-20"]#Towers
[font="sans-16"]
- Increase Sentry Towers Arrows from 3 to 4


[font="sans-bold-20"]#Civic Centre and Colony
[font="sans-16"]
- Adjust Civic Centre cost from 350/100 to 300/250 (Stone/Metal)
- Decrease Civic Centre loot reward from 100/100/100 to 60/60/50 (Wood/Stone/Metal)
- Increase Military Colony cost from 150/100 to 200/150 (Wood/Metal)
- Decrease Military Colony loot reward from 40/40/40 to 40/40/30 (Wood/Stone/Metal)
- Decrease Military Colony territory influence radius from 80 to 75


[font="sans-bold-20"]#Civilizations
[font="sans-16"]
-Ptolomies buildings are now easy to capture and has +50% building time
- Carthage team bonus: replaced to Mercenary Ranks: Mercenaries -50% training time
- Persian team bonus: -20% Barracks and Stable cost and build time
- Athens team bonus: replaced to Democracy: Civic Centre upgrades -50% research time and -30% cost
- Seleucids team bonus: Civic Centers and Colonies -20% cost and -30% building time


[font="sans-bold-20"]
#Version 2 (Oct 08, 2022)


[font="sans-bold-20"]#Bug fixes
[font="sans-16"]
- Fix the victory conditions infinite loop bug with a27 files.


[font="sans-bold-20"]
#Version 1 (Sep 28, 2022)

[font="sans-bold-20"]#Han Civ
[font="sans-16"]
- Fix Han rice tech not improving crops gather rates.